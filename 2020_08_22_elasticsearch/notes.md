# Course Notes

## Section 1: Installing and Understanding Elasticsearch

* Download tar archives from:
	- https://www.elastic.co/downloads/elasticsearch
	- https://www.elastic.co/downloads/kibana
* Configure elasticsearch:
	- config/elasticsearch.yml
	- set:
		* `node.name: node-1`
		* `discovery.seed_hosts: ["127.0.0.1"]`
		* `cluster.initial_master_nodes: ["node-1"]`
* Run elasticsearch with:
	```
	$ bin/elasticsearch
	```
* Test that it works with:
	```
	$ curl 127.0.0.1:9200
	```

Load some sample data:

* Download mapping: `$ wget http://media.sundog-soft.com/es7/shakes-mapping.json`
* Install: `curl -H "Content-Type: application/json" -XPUT 127.0.0.1:9200/shakespeare --data-binary @shakes-mapping.json`
* Download data: `$ wget http://media.sundog-soft.com/es7/shakespeare_7.0.json`
* Install: `curl -H "Content-Type: application/json" -XPUT 127.0.0.1:9200/shakespeare/_bulk --data-binary @shakespeare_7.0.json`

Search some data:

```
$ curl -H "Content-Type: application/json" -XGET '127.0.0.1:9200/shakespeare/_search?pretty' -d '{ "query": { "match_phrase": { "text_entry": "to be or not to be"}}}'

{
	"took" : 2298,
	"timed_out" : false,
	"_shards" : {
		"total" : 1,
		"successful" : 1,
		"skipped" : 0,
		"failed" : 0
	},
	"hits" : {
		"total" : {
			"value" : 1,
			"relation" : "eq"
		},
		"max_score" : 13.889601,
		"hits" : [
			{
				"_index" : "shakespeare",
				"_type" : "_doc",
				"_id" : "34229",
				"_score" : 13.889601,
				"_source" : {
					"type" : "line",
					"line_id" : 34230,
					"play_name" : "Hamlet",
					"speech_number" : 19,
					"line_number" : "3.1.64",
					"speaker" : "HAMLET",
					"text_entry" : "To be, or not to be: that is the question:"
				}
			}
		]
	}
}
```

### Basics

* Cluster:
	- think of cluster as a database
* Document:
	- like a row in a table
	- any structured data
	- every doc has unique ID
	- every doc has a type
* Index:
	- highest level of query
	- like a database table
	- only one type of document in an index
	- contains mappings that define schemas for the data within

### Term Frequency / Inverse Document Frequency (TF/IDF)

* An "index" is really an inverted index
* `TF-IDF` means "term frequency * inverse document frequency"
* `Term Frequency` is how often a term appears in a given document
* `Document Frequency` is how often a term appears in all documents
* `term frequency / document frequency` measures the relevance of a term in a document

### Using Elasticsearch

How to use an index

* REST API
* Client API
* Analytic Tools, like Kibana

### What's new in ES7

* Concept of document types is deprecated
* Elasticsearch SQL is "production ready"
* Lots of changes to defaults (ie, number of shards, replication)
* Lucene 8 under the hood
* Several X-Pack plugins now included with ES itself
* Bundled Java run-time
* Cross-cluster replication is "production ready"
* Index Life-cycle Management (ILM)
* High-level REST client in Java (HLRC)
* Lots of performance improvements
* Lots of small breaking changes

### How Elasticsearch Scales

* An index is split into shards
* Documents are hashed to a particular shard
* Each shard may be on a different node in a cluster
* Every shard is a self-contained Lucene index of its own

Primary and replica shards

* `Node` is really an instance of Elasticsearch
* Good idea to have an odd number of nodes for good resiliency

Primary shards

* Number of primary index shards cannot be changed
* To specify number of shards during index creating, specify settings:
	```
	PUT /testindex
	{
		"settings" : {
			"number_of_shards": 3,
			"number_of_replicas": 1
		}
	}
	```
* We end up with 6 shards with above config:
	- 3 primary shards
	- 1 replica for each primary
* As you cannot change the number of primary shards:
	- You can always add more read replicas afterwards, speeding up reading
	- Only writing that might suffer when scaling with too few primary shards
	- You can also re-index your data

## Section 2: Mapping and Indexing Data

### Introducing the MovieLens Data Set

* Download the recommended dev data set from https://grouplens.org/datasets/movielens/

### Analyzers

* A mapping is a schema definition

Common mappings

* Field types
	- String
	- byte
	- short
	- integer
	- long
	- float
	- double
	- boolean
	- date
	- sample:
		```
		"properties": {
			"user_id": {
				"type": "long"
			}
		}
		```
* Field Index
	- do you want full-text search index on field
	- you want analyzed/not_analyzed/no
	- sample:
		```
		"properties": {
			"genre": {
				"index": "not_analyzed"
			}
		}
		```
* Field Analyzer
	- define tokenizer and token filter
		- standard
		- whitespace
		- simple
		- english
		- etc
	- sample:
		```
		"properties": {
			"description": {
				"analyzer": "english"
			}
		}
		```

Analyzers

* Character filters - remove HTML encoding, convert '&' to 'and'
* Tokenizer - split string on whitespace/punctuation/non-letters
* Token filter - lowercaseing, stemming, synonyms, stopwords

Choices for analyzers:

* Standard - Splits on word boundaries, removes punctuation, lowercases. Good choice if language is unknown.
* Simple - Splits on anything that isn't a letter, and lowercase
* Whitespace - Splits on whitespace but doesn't lowercase
* Language (i.e. English) - Accounts for language-specific stopwords and stemming

### Import a Single Movie via JSON/REST

Custom curl script in `bin` directory, called `my_curl`.

```
$ bin/my_curl -XPUT 127.0.0.1:9200/movies -d '
{
	"mappings": {
		"properties": {
			"year": {
				"type": "date"
			}
		}
	}
}'
```

Check the mapping created: 

```
$ bin/my_curl 127.0.0.1:9200/movies/_mapping
```

Create movie:

```
$ bin/my_curl -XPOST 127.0.0.1:9200/movies/_doc/109487 -d '
{
	"genre": ["IMAX", "Sci-Fi"],
	"title": "Interstellar",
	"year": 2014
}'
```

Retrieve the created movie:

```
$ bin/my_curl -XGET 127.0.0.1:9200/movies/_search?pretty
```

### Insert Many Movies at Once with Bulk API

Get the data:

```
$ wget http://media.sundog-soft.com/es7/movies.json
```

Create multiple movies:

```
$ bin/my_curl -X PUT 127.0.0.1:9200/_bulk?pretty --data-binary @data/movies.json
```

### Updating Data

* Every document is immutable
* Every document has a `_version` field
* Doc ID + `_version` is unique
* Old doc is marked for deletion

```
$ bin/my_curl -X PUT 127.0.0.1:9200/movies/_doc/109487?pretty -d '
{
	"genres": ["IMAX", "Sci-Fi"],
	"title": "Interstellar v2",
	"year": 2014
}'
```

Check that change was made:

```
$ bin/my_curl -XGET 127.0.0.1:9200/movies/_search?pretty
```

* `POST` with `_update` for partial updates
* `PUT` for full document updates

```
$ bin/my_curl -X POST 127.0.0.1:9200/movies/_doc/109487/_update -d '
{
	"doc": {
		"title": "Interstellar v3"
	}
}'
```

### Deleting Data

Search for the movie ID:

```
$ bin/my_curl -XGET "127.0.0.1:9200/movies/_search?pretty&q=Dark"
```

Now, delete it:

```
$ bin/my_curl -X DELETE 127.0.0.1:9200/movies/_doc/58559?pretty
```

### Exercise: insert, update, and delete a movie

Insert:

```
$ bin/my_curl -XPOST 127.0.0.1:9200/movies/_doc/100 -d '
{
	"genre": ["Horror", "Sci-Fi"],
	"title": "Alien",
	"year": 1979
}'
```

Show:

```
$ bin/my_curl -XGET "127.0.0.1:9200/movies/_search?pretty&q=Alien"
```

Update (partial):

```
$ bin/my_curl -X POST 127.0.0.1:9200/movies/_doc/100/_update -d '
{
	"doc": {
		"title": "Alien v2"
	}
}'
```

Delete:

```
$ bin/my_curl -X DELETE 127.0.0.1:9200/movies/_doc/100?pretty
```

### Dealing with Concurrency

Optimistic Concurrency Control:

* Every value is associated with
	- `_seq_no`
	- `_primary_term` - shard?
* Use option `retry_on_conflicts` to automatically retry on failure

Update doc and restrict update to specific sequence and primary term:

```
$ bin/my_curl -X PUT "127.0.0.1:9200/movies/_doc/109487?if_seq_no=6&if_primary_term=2" -d '
{
	"genres": ["IMAX", "Sci-Fi"],
	"title": "Interstellar Foo",
	"year": 2014
}'
```

Automatic conflict resolution:

```
$ bin/my_curl -X POST "127.0.0.1:9200/movies/_doc/109487/_update?retry_on_conflict=5" -d '
{
	"doc": {
		"title": "Interstellar typo"
	}
}'
```

### Using Analyzers and Tokenizers

* Sometimes text fields should be exact-match
	- Use `keyword` mapping instead of `text`
	- Will suppress analyzing for that field and allow only exact matches
* Search on analyzed `text` fields will reutnr anything remotely relevent
	- Depending on the analyzer, results will be case-insensitive, stemmed, stopwords removed, synonyms applied, etc.
	- Searches with multiple terms need not match them all

```
$ bin/my_curl -X GET 127.0.0.1:9200/movies/_search?pretty -d '
{
	"query": {
		"match": {
			"title": "Star Trek"
		}
	}
}'
```

Search on genres:

```
$ bin/my_curl -X GET 127.0.0.1:9200/movies/_search?pretty -d '
{
	"query": {
		"match_phrase": {
			"genre": "sci"
		}
	}
}'
```

Re-create index as you cannot change mappings after creation. Delete the entire `movies` index:

```
$ bin/my_curl -X DELETE 127.0.0.1:9200/movies
```

Create new mapping for `movies` index, specifying `genres` as `keyword`, requiring exact matches:

```
$ bin/my_curl -X PUT 127.0.0.1:9200/movies -d '
{
	"mappings": {
		"properties": {
			"id": { "type": "integer" },
			"year": { "type": "date" },
			"genre": { "type": "keyword" },
			"title": { "type": "text", "analyzer": "english" }
		}
	}
}'
```

Load data:

```
$ bin/my_curl -X PUT 127.0.0.1:9200/_bulk?pretty --data-binary @data/movies.json
```

Retry "sci" on genre:

```
$ bin/my_curl -X GET 127.0.0.1:9200/movies/_search?pretty -d '
{
	"query": {
		"match_phrase": {
			"genre": "sci"
		}
	}
}'
```

### Data Modeling and Parent/Child Relationships, Part 2

Create new index `series`:

```
$ bin/my_curl -X PUT 127.0.0.1:9200/series -d '
{
	"mappings": {
		"properties": {
			"film_to_franchise": {
				"type": "join",
				"relations": { "franchise": "film" }
			}
		}
	}
}'
```

Download series data:

```
$ wget http://media.sundog-soft.com/es7/series.json
```

Bulk load the series data:

```
$ bin/my_curl -X PUT 127.0.0.1:9200/_bulk?pretty --data-binary @data/series.json
```

Get all films of "Star Wars" franchise:

```
$ bin/my_curl -X GET 127.0.0.1:9200/series/_search?pretty -d '
{
	"query": {
		"has_parent": {
			"parent_type": "franchise",
			"query": {
				"match": {
					"title": "Star Wars"
				}
			}
		}
	}
}'
```

Find franchise associated with a film:

```
$ bin/my_curl -X GET 127.0.0.1:9200/series/_search?pretty -d '
{
	"query": {
		"has_child": {
			"type": "film",
			"query": {
				"match": {
					"title": "The Force Awakens"
				}
			}
		}
	}
}'
```

### Flattened Datatype

Get commands from: http://media.sundog-soft.com/es/flattened.txt

```
$ bin/my_curl -X PUT "http://127.0.0.1:9200/demo-default/_doc/1" -d'{
  "message": "[5592:1:0309/123054.737712:ERROR:child_process_sandbox_support_impl_linux.cc(79)] FontService unique font name matching request did not receive a response.",
  "fileset": {
    "name": "syslog"
  },
  "process": {
    "name": "org.gnome.Shell.desktop",
    "pid": 3383
  },
  "@timestamp": "2020-03-09T18:00:54.000+05:30",
  "host": {
    "hostname": "bionic",
    "name": "bionic"
  }
}'
```

Show the mapping of the `demo-default` index:

```
$ bin/my_curl -X GET "http://127.0.0.1:9200/demo-default/_mapping?pretty=true"
```

Show the cluster state:

```
$ bin/my_curl -X GET "http://127.0.0.1:9200/_cluster/state?pretty=true" >> es-cluster-state.json
```

* Flattened datatype maps entire object, along with its inner fields into a single field.
* Inner fields don't appear in the mappings at all.

Create new index:

```
$ bin/my_curl -X PUT 127.0.0.1:9200/demo-flattened
```

Specify flattened type for the host field:

```
$ bin/my_curl -X PUT "http://127.0.0.1:9200/demo-flattened/_mapping" -d '
{
	"properties": {
		"host": {
			"type": "flattened"
		}
	}
}'
```

```
$ bin/my_curl -X PUT "http://127.0.0.1:9200/demo-flattened/_doc/1" -d '
{
	"message": "[5592:1:0309/123054.737712:ERROR:child_process_sandbox_support_impl_linux.cc(79)] FontService unique font name matching request did not receive a response.",
	"fileset": {
		"name": "syslog"
	},
	"process": {
		"name": "org.gnome.Shell.desktop",
		"pid": 3383
	},
	"@timestamp": "2020-03-09T18:00:54.000+05:30",
	"host": {
		"hostname": "bionic",
		"name": "bionic"
	}
}'
```

```
$ bin/my_curl -X GET "http://127.0.0.1:9200/demo-flattened/_mapping?pretty=true"
```

* Elasticsearch stores flattened datatype values as keywords
* Numerical calculation or full text matching cannot be applied to the flattened fields or their inner fields

Supported queries for flattened datatype:

* term, terms and terms_set
* prefix
* range (non numerical range operations)
* match and multi_match (we have to supply exact keywords)
* query_string and simple_query_string
* exists

Search highlighting capability also not supported for flattened types, as they are not analyzed.

### Dealing with Mapping Exceptions

See chapter cheat sheet at media.sundog-soft.com/es/exceptions.txt

How to fix mapping exceptions?

* No magic solution
* Partially solve problem by defining an ignore malformed mapping parameter
	- close index
	- set parameter
	- open index

Close index:

```
bin/my_curl --request POST 'http://localhost:9200/microservice-logs/_close'
```

Set ignore setting:

```
bin/my_curl --location --request PUT 'http://localhost:9200/microservice-logs/_settings' \
--data-raw '{
   "index.mapping.ignore_malformed": true
}'
```

Open the index:

```
bin/my_curl --request POST 'http://localhost:9200/microservice-logs/_open'
```

* Ignoring malformed input does not work on JSON input
* Another solution is to introduce a dead letter queue. Failed documents will be stored in another queue.
	- Either on application level, or
	- by applying Logstash DLQ

## Section 3: Searching with Elasticsearch

### "Query Lite" interface

* Formally known as URI search
* Compact URS:
	- /movies/_search?q=title:star
	- /movies/_search?q=+year:>2010+title:trek

Reasons not to use Query Lite in production:

* Cryptic and tough to debug
* Can be a security issue if exposed to end users
* Fragile - one wrong character and you're hosed

### JSON Search In-Depth

* Query DSL is in the request body as JSON
* Yes, a GET request can have a body

Queries and Filters:

* Filters ask a yes/no question of your data
* Queries return data in terms of relevance
* Use filters when you can - they are faster and cacheable

Equivalent of an "and" query is a "bool" expression with "must".

Kinds of filters:

* `Term`: filter by exact values: `{ "term": { "year": 2014 }}`
* `Terms`: match if any exact values in a list match: `{ "terms": { "genre": ["Sci-Fi", "Adventure" ]}}`
* `Range`: find numbers or dates in a given range (gt, gte, lt, lte):
	`{ "range": { "year": { "gte": 2010 }}}`
* `Exists`: find documents where a field exists: `{ "exists": { "field": "tags" }}`
* `Missing`: find documents where a field is missing: `{ "missing": { "field": "tags" }}`
* `Bool`: combine filters with boolean logic (must, must_not, should)
	- `must` is like "and"
	- `should` is like "or"

Some types of queries:

* `Match_all`: Returns all documents and is the default. Normally used with a filter.
	`{ "match": {}}`
* `Match`: Searches analyzed results, such as full text search.
	`{ "match": { "title": "star" }}`
* `Multi_match`: Run the same query on multiple fields.
	`{ "multi_match": { "query": "star", "fields": ["title", "synopsis" ]}}`
* `Bool`: Works like a bool filter, but results are scored by relevance.

Syntax reminder:

* Queries are wrapped in a `"query": { }` block
* Filters are wrapped in a `"filter": { }` block
* You can combine filters inside queries, or queries inside filters too.

Practice:

```
bin/my_curl 127.0.0.1:9200/movies/_search?pretty -d '
{
	"query": {
		"match": {
			"title": "star"
		}
	}
}'

bin/my_curl 127.0.0.1:9200/movies/_search?pretty -d '
{
	"query": {
		"bool": {
			"must": { "term": { "title": "trek" }},
			"filter": { "range": { "year": { "gte": 2010 }}}
		}
	}
}'
```

### JSON Search In-Depth

* GET requests can have a body
* Filters ask a yes/no question of your data
	- results are cached by Elasticsearch
* Queries return data in terms of relevance
* Some types of filters:
	- exact value: `{ "term": { "year": 2014 }}`
	- match if any exact values in a list match: `{ "term": { "genre": ["Sci-Fi", "Adventure"] }}`
	- find numbers or dates in a given range (gt, gte, lt, lte): `{ "range": { "year": { "gte" 2010 }}}`
	- find documents where a field exists: `{ "exists": { "field": "tags" }}`
	- find documents where a field is missing: `{ "missing": { "field": "tags" }}`
	- combine filters with Boolean logic (must, must_not, should)
* Some types of queries:
	- Returns all documents and is the default. Normally used with a filter. `{ "match_all": {}}`
	- searches analyzed results, such as full text search: `{ "match": { "title": "star" }`
	- run the same query on multiple fields: `{ "multi_match": { "query": "star", "fields": ["title", "synopsis"] }}`
	- `bool` works like a bool filter, but results are scored by relevance
* Queries are wrapped in a `query` block and filter are wrapped in a `filter` block

```
bin/my_curl 127.0.0.1:9200/movies/_search?pretty -d '
{
	"query": {
		"match": {
			"title": "star"
		}
	}
}'

/usr/bin/curl -H "Content-type: application/json" 127.0.0.1:9200/movies/_search?pretty -d '{ "query": { "match_all": {}}}'

bin/my_curl 127.0.0.1:9200/movies/_search?pretty -d '
{
	"query": {
		"bool": {
			"must": {
				"term": { "title": "trek" }
			},
			"filter": {
				"range": { "year": { "gte": 2010 }}
			}
		}
	}
}'
```

### Phrase Matching

* Order matters
* `slop` represents how far you're willing to let a term move to satisfy a phrase (in either direction)
* Example: "quick brown fox" would match "quick fox" with `slop` of 1

```
bin/my_curl 127.0.0.1:9200/movies/_search?pretty -d '
{
	"query": {
		"match_phrase": {
			"title": { "query": "star beyond", "slop": 1 }
		}
	}
}'
```

Proximity Queries:

* Use a really high `slop` if you want to get any documents that contain the words in your phrase, but want documents that have the words closer together score higher.

```
bin/my_curl 127.0.0.1:9200/movies/_search?pretty -d '
{
	"query": {
		"match_phrase": {
			"title": { "query": "star beyond", "slop": 1 }
		}
	}
}'
```

### Pagination

Syntax:

```
bin/my_curl -X GET "127.0.0.1:9200/movies/_search?size=2&from=2&pretty"

bin/my_curl -X GET 127.0.0.1:9200/movies/_search?pretty -d '
{
	"from": 2,
	"size": 2,
	"query": {
		"match": {
			"genre": "Sci-Fi"
		}
	}
}'
```

* Deep pagination can kill performance
* Every result must be retrieved, collected, and sorted
* Enforce an upper bound on how many results you'll return to users

```
bin/my_curl -X GET "127.0.0.1:9200/movies/_search?size=2&from=2&pretty"
```

### Sorting

Syntax:

```
bin/my_curl -X GET "127.0.0.1:9200/movies/_search?sort=year&pretty"
```

* A string field that is "analyzed" for full-text search can't be used to sort documents, as it exists in the inverted index as individual terms, not as the entire string.
* A way around this is to duplicate the property as `raw`, which is not analyzed.
* Need to reindex data in order to create duplicate `raw` field that we can use to sort the title on.

```
bin/my_curl -X DELETE 127.0.0.1:9200/movies

bin/my_curl -X PUT "127.0.0.1:9200/movies/" -d '
{
	"mappings": {
		"properties": {
			"id": { "type": "integer" },
			"year": { "type": "date" },
			"genre": { "type": "keyword" },
			"title": {
				"type": "text",
				"analyzer": "english",
				"fields": {
					"raw": {
						"type": "keyword"
					}
				}
			}
		}
	}
}' | jq
```

### More with Filters

Sample filter:

```
bin/my_curl -X GET 127.0.0.1:9200/movies/_search -d '
{
	"query": {
		"bool": {
			"must": { "match": { "genre": "Sci-Fi" }},
			"must_not": { "match": { "title": "trek" }},
			"filter": { "range": { "year": { "gte": 2010, "lt": 2015 }}}
		}
	}
}' | jq
```

### Exercise - Using Filters

```
bin/my_curl -X GET 127.0.0.1:9200/movies/_search -d '
{
	"query": {
		"bool": {
			"must": { "match": { "genre": "Sci-Fi" }},
			"filter": { "range": { "year": { "lt": 1960 }}}
		}
	},
	"sort": "title.raw"
}' | jq
```

### Fuzzy Queries

* A way to account for typos and misspellings
* The _levenshtein edit distance_ accounts for:
	- substitutions of characters
	- insertion of characters
	- deletion of character

Sample:

```
bin/my_curl -X GET 127.0.0.1:9200/movies/_search -d '
{
	"query": {
		"fuzzy": {
			"title": {
				"value": "intresteller",
				"fuzziness": 2
			}
		}
	}
}'
```

_AUTO_ fuzziness

* `0` for 1-2 character strings
* `1` for 3-5 characters strings
* `2` for anything else

```
bin/my_curl 127.0.0.1:9200/movies/_search -d '
{
	"query": {
		"match": {
			"title": "intersteller"
		}
	}
}' | jq

bin/my_curl 127.0.0.1:9200/movies/_search -d '
{
	"query": {
		"fuzzy": {
			"title": {
				"value": "intersteller",
				"fuzziness": 1
			}
		}
	}
}' | jq
```

### Partial Matching

* Works on _string_ fields

```
# delete the "movies" index
bin/my_curl -X DELETE 127.0.0.1:9200/movies

# create the index
bin/my_curl -X PUT "127.0.0.1:9200/movies/" -d '
{
	"mappings": {
		"properties": {
			"id": { "type": "integer" },
			"year": { "type": "text" },
			"genre": { "type": "keyword" },
			"title": {
				"type": "text",
				"analyzer": "english",
				"fields": {
					"raw": {
						"type": "keyword"
					}
				}
			}
		}
	}
}' | jq

# bulk insert our data
bin/my_curl -X PUT 127.0.0.1:9200/_bulk --data-binary @data/movies.json | jq

```

Prefix queries:

```
bin/my_curl -X GET 127.0.0.1:9200/movies/_search -d '
{
	"query": {
		"prefix": {
			"year": "201"
		}
	}
}' | jq
```

Wildcard queries:

```
bin/my_curl -X GET 127.0.0.1:9200/movies/_search -d '
{
	"query": {
		"wildcard": {
			"year": "1*"
		}
	}
}' | jq
```

### Query-time Search As You Type

* Use `match_phrase_prefix`. Like _match prefix_, but works on the phrase level.

Sample:

```
bin/my_curl -X GET 127.0.0.1:9200/movies/_search -d '
{
	"query": {
		"match_phrase_prefix": {
			"title": {
				"query": "star trek",
				"slop": 10
			}
		}
	}
}' | jq
```

### N-Grams, Part 1

Index-time with N-grams

Using "star":

* unigram: [ s, t, a, r ]
* bigram: [ st, ta, ar ]
* trigram: [ sta, tar ]
* 4-gram: [ star ]

* Edge N-grams are built only on the beginning of each term
* Create completion suggesters to help with autocomplete

### N-Grams, Part 2

```
# delete the "movies" index
bin/my_curl -X DELETE 127.0.0.1:9200/movies | jq

# create custom filter
bin/my_curl -X PUT "127.0.0.1:9200/movies/" -d '
{
	"settings": {
		"analysis": {
			"filter": {
				"autocomplete_filter": {
					"type": "edge_ngram",
					"min_gram": 1,
					"max_gram": 20
				}
			},
			"analyzer": {
				"autocomplete": {
					"type": "custom",
					"tokenizer": "standard",
					"filter": [ "lowercase", "autocomplete_filter" ]
				}
			}
		}
	}
}' | jq

# To test, explicitly analyze a string
bin/my_curl -X GET 127.0.0.1:9200/movies/_analyze -d '
{
	"analyzer": "autocomplete",
	"text": "sta"
}
' | jq

# create the index
bin/my_curl -X PUT "127.0.0.1:9200/movies/_mappings" -d '
{
	"properties": {
		"id": { "type": "integer" },
		"year": { "type": "text" },
		"genre": { "type": "keyword" },
		"title": {
			"type": "text",
			"analyzer": "autocomplete"
		}
	}
}' | jq

# bulk insert our data
bin/my_curl -X PUT 127.0.0.1:9200/_bulk --data-binary @data/movies.json | jq

# try out the autocomplete analyzer
bin/my_curl -X GET 127.0.0.1:9200/movies/_search -d '
{
	"query": {
		"match": {
			"title": "sta"
		}
	}
}' | jq

# query again, buth with standard analyzer
bin/my_curl -X GET 127.0.0.1:9200/movies/_search -d '
{
	"query": {
		"match": {
			"title": {
				"query": "sta",
				"analyzer": "standard"
			}
		}
	}
}' | jq

```

### "Search as you type" Field Type

Autocomplete with "search as you type"

From: http://media.sundog-soft.com/es/sayt.txt

```
bin/my_curl -X POST 'http://localhost:9200/movies/_analyze?pretty' \
--data-raw '{
   "tokenizer" : "standard",
   "filter": [{"type":"edge_ngram", "min_gram": 1, "max_gram": 4}],
   "text" : "Star"
}' | jq


bin/my_curl -X PUT 'http://localhost:9200/autocomplete' \
-d '{
   "mappings": {
       "properties": {
           "title": {
               "type": "search_as_you_type"
           },
           "genre": {
               "type": "search_as_you_type"
           }
       }
   }
}' | jq


bin/my_curl -X POST 'http://localhost:9200/_reindex?pretty' --data-raw '
{
 	"source": {
   		"index": "movies"
 	},
 	"dest": {
   		"index": "autocomplete"
 	}
}' | grep "total\|created\|failures"

# show the mapping
bin/my_curl -X GET 127.0.0.1:9200/autocomplete/_mapping | jq

```

`bool_prefix` type can match the searched words in any order, but assigns a higher score to words in the same order as the query.

```
bin/my_curl -X GET 'http://localhost:9200/autocomplete/_search?pretty' --data-raw '
{
   "size": 5,
   "query": {
       "multi_match": {
           "query": "Sta",
           "type": "bool_prefix",
           "fields": [
               "title",
               "title._2gram",
               "title._3gram"
           ]
       }
   }
}' | jq


while true
do
	IFS= read -rsn1 char
	INPUT=$INPUT$char
	echo $INPUT
	bin/my_curl -X GET 'http://localhost:9200/autocomplete/_search' -d '
 	{
    	"size": 5,
    	"query": {
    		"multi_match": {
        		"query": "'"$INPUT"'",
        		"type": "bool_prefix",
        		"fields": [
        			"title",
            		"title._2gram",
            		"title._3gram"
        		]
        	}
		}
	}' | jq .hits.hits[]._source.title | grep -i "$INPUT"
done
```

## Section 4: Importing Data into your Index - Big or Small

### 46. Importing Data with a Script

Get the Python script:

```
$ wget media.sundog-soft.com/es7/MoviesToJson.py
```

Convert the movies CSV file to JSON:

```
$ python3 MoviesToJson.py > moremovies.json
```

Bulk import the data:

```
$ bin/my_curl -X PUT 127.0.0.1:9200/_bulk --data-binary @data/moremovies.json
```

Some search:

```
$ bin/my_curl -X GET "127.0.0.1:9200/movies/_search?q=mary%20poppins" | jq
```

### 47. Importing with Client Libraries

Install Pyton pip:

```
$ sudo apt install python3-pip
```

Install Python elasticsearch module:

```
$ pip3 install elasticsearch
```

Download python script:

```
$ wget http://media.sundog-soft.com/es7/IndexRatings.py
```

Run ratings script:

```
$ python3 IndexRatings.py
```

### Introducing Logstash

* Logstash parses, transforms, and filters data as it passes through
* It can derive structure from unstructured data
* it can anonymize personal data or exclude it entirely
* It can do geo-location lookups
* It can scale across many nodes
* It guarantees at-least-once delivery
* It absorbs throughput from load spikes

### Running Logstash

List indexes in Elasticsearch:

```
bin/my_curl -X GET 127.0.0.1:9200/_cat/indices?v
```

### Logstash and MySQL, Part 1

Load CSV file into table:

```
$ mysql -u root -p
mysql> load data local infile '/path/to/file.csv' into table database.table fields terminated by '|' (movieID, title, @var3) set releaseDate = STR_TO_DATE(@var3, '%d-%M-%Y');
```

### Logstash and MySQL, Part 2

Handy MySQL stuff:

```
mysql> create user 'student'@'localhost' identified by 'password';
mysql> grant all privileges on *.* to 'student'@'localhost';
mysql> flush privileges;
```

### Logstash Grok Examples for Common Log Formats

Handy grok debug app:

https://grokdebug.herokuapp.com

### Elasticsearch and Kafka, Part 1

* install Zookeeper: `$ sudo apt install zookeeperd`
* download Kafka from https://kafka.apachr.org/downloads
* run Kafka: `$ sudo bin/kafka-server-start.sh config/server.properties`
* create topic:
	```
	$ sudo bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic kafka-logs
	```
* publish data to topic:
	```
	$ sudo bin/kafka-console-producer.sh --broker-list localhost:9092 --topic kafka-logs < ../access.log
	```

## Section 5: Aggregation

### 72. Aggregations, Buckets, and Metrics

```
$ bin/my_curl -X GET "127.0.0.1:9200/ratings/_search?size=0" -d '
{
	"aggs": {
		"ratings": {
			"terms": {
				"field": "rating"
			}
		}
	}
}'

$ bin/my_curl -X GET "127.0.0.1:9200/ratings/_search?size=0" -d '
{
	"query": {
		"match": {
			"rating": 5.0
		}
	},
	"aggs": {
		"ratings": {
			"terms": {
				"field": "rating"
			}
		}
	}
}' | jq

$ bin/my_curl -X GET "127.0.0.1:9200/ratings/_search?size=0" -d '
{
	"query": {
		"match_phrase": {
			"title": "Star Wars Episode IV"
		}
	},
	"aggs": {
		"avg_rating": {
			"avg": {
				"field": "rating"
			}
		}
	}
}' | jq


$ bin/my_curl -X GET "127.0.0.1:9200/ratings/_search?size=0" -d '
{
	"query": {
		"match": {
			"title": "Star Wars"
		}
	},
	"aggs": {
		"avg_rating": {
			"avg": {
				"field": "rating"
			}
		}
	}
}' | jq
```

### 73. Histograms

* A histogram is displays totals of documents, bucketed by some interval range

```
# below, the "whole_ratings" property is the name of our aggregate
bin/my_curl -X GET "127.0.0.1:9200/ratings/_search?size=0" -d '
{
	"aggs": {
		"whole_ratings": {
			"histogram": {
				"field": "rating",
				"interval": 1.0
			}
		}
	}
}' | jq

bin/my_curl -X GET "127.0.0.1:9200/movies/_search?size=0" -d '
{
	"aggs": {
		"release": {
			"histogram": {
				"field": "year",
				"interval": 10
			}
		}
	}
}' | jq
```

### 74. Time Series

* Time series is data over time
* Elasticsearch can bucket and aggregate fielsd that contain time and dates properly. You can aggregate by "year" or "month" and it knows about calendar rules.

```
bin/my_curl -X GET "127.0.0.1:9200/kafka-logs/_search?size=0" -d '
{
	"aggs": {
		"timestamp": {
			"date_histogram": {
				"field": "@timestamp",
				"interval": "hour"
			}
		}
	}
}' | jq

# traffic from "Googlebot"
# to find out what time of day Google typically scrapes the site
bin/my_curl -X GET "127.0.0.1:9200/kafka-logs/_search?size=0" -d '
{
	"query": {
		"match": {
			"agent": "Googlebot"
		}
	},
	"aggs": {
		"the_timestamp": {
			"date_histogram": {
				"field": "@timestamp",
				"interval": "hour"
			}
		}
	}
}' | jq
```

### 75. Exercise - Generating Histogram Data

Find out when an outage on our website occurred. We'll be looking for status code 500 and using a "minute" interval.

```
bin/my_curl -X GET "127.0.0.1:9200/kafka-logs/_search?size=0" -d '
{
	"query": {
		"match": {
			"response": "500"
		}
	},
	"aggs": {
		"outage_timestamp": {
			"date_histogram": {
				"field": "@timestamp",
				"interval": "minute"
			}
		}
	}
}' | jq
```

### 76. Nested Aggregations, Part 1

* Aggregations can be nested for more powerful queries

Final query:

```
bin/my_curl -X GET "127.0.0.1:9200/ratings/_search?size=0" -d '
{
	"query": {
		"match_phrase": {
			"title": "Star Wars"
		}
	},
	"aggs": {
		"titles": {
			"terms": {
				"field": "title.raw"
			},
			"aggs": {
				"avg_ratings": {
					"avg": {
						"field": "rating"
					}
				}
			}
		}
	}
}' | jq

# set fielddata
bin/my_curl -X PUT 127.0.0.1:9200/ratings/_mapping -d '
{
	"properties": {
		"title": {
			"type": "text",
			"fielddata": true
		}
	}
}' | jq

# delete the ratings index
bin/my_curl -X DELETE 127.0.0.1:9200/ratings | jq

# create ratings mappings
bin/my_curl -X PUT 127.0.0.1:9200/ratings -d '
{
	"mappings": {
		"properties": {
			"title": {
				"type": "text",
				"fielddata": true,
				"fields": {
					"raw": {
						"type": "keyword"
					}
				}
			}
		}
	}
}' | jq

# populate the ratings index
python3 IndexRatings.py

# check the ratings data
bin/my_curl -X GET 127.0.0.1:9200/ratings/_mapping | jq

# try the nested aggs again
bin/my_curl -X GET "127.0.0.1:9200/ratings/_search?size=0" -d '
{
	"query": {
		"match_phrase": {
			"title": "Star Wars"
		}
	},
	"aggs": {
		"titles": {
			"terms": {
				"field": "title.raw"
			},
			"aggs": {
				"avg_ratings": {
					"avg": {
						"field": "rating"
					}
				}
			}
		}
	}
}' | jq
```

## Section 6: Using Kibana

### 80. Installing Kibana

http://127.0.0.1:5601

### 84. Kibana Management

```
curl localhost:5601/api/spaces/space/devops
```

Get: https://raw.githubusercontent.com/elastic/uptime-contrib/master/dashboards/http_dashboard.json
