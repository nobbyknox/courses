# Notes

## Resources

* https://github.com/quarkus-course
* https://docs.docker.com/engine/install/ubuntu
* https://quarkus.io/guides/all-config

---

```shell
# This does not work
mvn io.quarkus:quarkus-maven-plugin:1.2.0.Final:create \
	-DprojectGroupId=tech.donau.course \
	-DprojectArtifactId=getting-started \
	-DclassName="tech.donau.course.GreetingResource" \
	-Dpath="/hello"

# This one works
mvn io.quarkus:quarkus-maven-plugin:1.9.2.Final:create \
    -DprojectGroupId=org.acme \
    -DprojectArtifactId=getting-started \
    -DclassName="org.acme.getting.started.GreetingResource" \
    -Dpath="/hello"
```

## Section 2

### Lesson 10

To mock a service, add the `@Mock` annotation to class and extend the service to mock, like this:

```java
@Mock
@ApplicationScoped
public class MockGreetingService extends GreetingService {
	@Override
	public String sayHello(String name) {
		// impl here
	}
}
```

Question: How is the mock injected?

## Section 3

### Lesson 13 - Configuring Your Application

* application.properties

Sample:

```properties
greeting.name=abc
greeting.number=123
```

```java
@ApplicationScoped
public class GreetingService {
	@ConfigProperty(name = "greeting.name")
	String name;

	@ConfigProperty(name = "greeting.number")
	Integer number;

	@ConfigProperty(name = "greeting.suffix", defaultValue = "!")
	String suffix;

	@ConfigProperty(name = "greeting.prefix")
	Optional<String> prefix;

	public String sayHello() {
		final String name2 = ConfigProvider.getConfig().getValue("greeting.name", String.class);
	}
}
```

Run app:

```shell
$ ./mvnw quarkus:dev
```

---

Create config class:

```java
@Data
@ConfigProperties(prefix = "greeting")
public class GreetingConfig {
	String name;
	String number;
	String suffix = "default value";
	Optional<String> prefix;
}
```

To specify property values for other profiles, prefix the property name with `%profile.` in application.properties. Sample:

```properties
greeting.name=abc
greeting.number=123
%test.greeting.name=abc
%test.greeting.number=123
```

### Lesson 14 - Custom Configuration Converters

Check out this chapter

### Lesson 17 - Configuring Logging

Properties relevant to logging in `application.properties`.

```properties
quarkus.log.level=DEBUG
quarkus.log.category."com.nobbyknox.testproject".level=DEBUG
quarkus.log.console.enable=false

# Add %L to print the line number in the file where the log message was printed
quarkus.log.console.format=%d{yyyy-MM-dd HH:mm:ss,SSS} %-5p [%c{3.}] (%t) %s%e%n

quarkus.log.file.enable=true
quarkus.log.file.path=./project.log

# Log to JSON
# Need the io.quarkus > quarkus-logging-json dependency
quarkus.log.console.json=true
```

* Default log level is `INFO`.

## Section 4 - Quarkus Web

### Lesson 19 - Validation with Hibernate Validator

* Add dependency `io.quarkus` > `quarkus-hibernate-validator`
* I like the term `violation` when referring to business rule violations.

### Lesson 20 - Using the REST Client

In application.properties

```properties
com.nobbyknox.project.CurrencyService/mp-rest/uri=https://some.domain
com.nobbyknox.project.CurrencyService/mp-rest/scope=javax.inject.Singleton
```

or add `@RegisterRestClient(configKey = "config.api.crypto") to a class/interface and change the properties to:

```properties
config.api.crypto/mp-rest/uri=https://some.domain
config.api.crypto/mp-rest/scope=javax.inject.Singleton
```

### Lesson 23 - OpenAPI and Swagger UI

* Add dependency:
	- `./mvnw quarkus:add-extension -Dextensions="openapi"`, or in pom.xml
	- `io.quarkus` > `quarkus-smallrye-openapi`
* Run your app and access the OpenAPI YAML on http://127.0.0.1:8080/openapi
* You can change the OpenAPI path by adding this property:
	- quarkus.smallrye-openapi.path=/newpath
* More properties:
	```properties
	quarkus.smallrye-openapi.path=/swagger
	mp.openapi.scan.disable=true
	quarkus.swagger-ui.always-include=true
	quarkus.swagger-ui.path=/public-api
	```
* Configure OpenAPI by annotating a class that extends javax.ws.rs.core.Application, like this:
	```java
	@OpenAPIDefinition(
		info = @Info(
			title = "Getting Started with OpenAPI",
			description = "Some description",
			version = "1.2"
		)
	)
	public class QuarkusApplication extends Application {
	}
	```
* Swagger UI is available on http://127.0.0.1:8080/swagger-ui

### Lesson 24 - Setting up HTTP in Quarkus

* To change the root path, add this property:
	- `quarkus.http.root-path=/api`
* Body size is unlimited in Quarkus
* To limit body and header sizes, add properties:
	- `quarkus.http.limits.max-body-size=3M`
	- `quarkus.http.limits.max-header-size=12M`
* More properties:
	- `quarkus.servlet.servlet-path=/server`

### Lesson 25 - HTTPS, Letsencrypt, Quarkus

* Install certbot: `sudo apt install certbot`, then
* `certbot certonly -a manual --preferred-challenges dns -d your.domain.com`
* Prepare certificate for Quarkus:
	```
	openssl pkcs12 -export \
		-out /etc/letsencrypt/live/your.domain.com/bundle.pfx \
		-inkey /etc/letsencrypt/live/your.domain.com/privkey.pem \
		-in /etc/letsencrypt/live/your.domain.com/cert.pem \
		-certfile /etc/letsencrypt/live/your.domain.com/chain.pem \
		-password pass:testtest22
	```
* Add properties:
	```properties
	quarkus.http.port=80
	quarkus.http.ssl-port=443
	quarkus.http.ssl.certificate.key-store-file=/etc/letsencrypt/live/your.domain.com/bundle.pfx
	quarkus.http.ssl.certificate.key-store-file-type=PKCS12
	quarkus.http.ssl.certificate.key-store-password=testtest22
	```

### Lesson 26 - CORS - Cross-Origin Resource Sharing

```properties
# some application.properties used in this lecture
quarkus.http.cors=true
#quarkus.http.cors.origins=http://localhost:63342,http://localhost:63345
#quarkus.http.cors.methods=GET
#quarkus.http.cors.headers=X-Custom
#quarkus.http.cors.exposed-headers=Content-Description
#quarkus.http.cors.access-control-max-age=12H
```

### Lesson 27 - Fault Tolerance

* On REST methods (endpoints), add
	- `@Retry(maxRetries = 4, delay = 1000L)` to retry endpoints
	- `@Timeout` to specify timeout of endpoint (fail if timed out)
	- `@Fallback` specify fallback handler
	- `@CircuitBreaker(failureRatio = 0.5, failOn = RuntimeException.class, delay = 20000L)`

### Lesson 28 - Using Reactive Routes

* https://github.com/quarkus-course/lecture-reactive-routes
* Add dependency: `io.quarkus` > `quarkus-vertx-web`

```java
@ApplicationScoped
@RouteBase(path = "/base", produces = "text/html")
public class GreetingResource {
	@Route(path = "/some/path", methods = HttpMethod.GET)
	public void hello(RoutingContext rc) {
		rc.response().end("hello");
	}

	public void init(@Observes Router router) {
        router.get("/book").handler(rc -> rc.response().end("Book1"));
    }
}
```

## Section 5 - Maven, Gradle and Native Applications in Quarkus

### Lesson 30 - Building and Inspecting Project with Maven

* List all dependencies: `./mvnw quarkus-bootstrap:build-tree`

## Section 6 - Quarkus Data

### Lesson 34 - Configuring your Datasources

* Add dependency: `io.quarkus` > `quarkus-agroal`
	- Why do I need it? Update: Maybe database pool.
* Relevant properties:
	```properties
	quarkus.datasource.driver=org.h2.Driver
	quarkus.datasource.url=jdbc:h2:tcp://localhost/mem:default
	quarkus.datasource.username=username
	quarkus.datasource.min-size=3
	quarkus.datasource.max-size=13
	```
* SQL tip: Easier/faster inserts:
	```sql
	insert into <table> values (1, 'hello');
	```
* You can have multiple datasources
	- Define properties like this:
	```properties
	# Datasource name is "hello"
	quarkus.datasource.hello.driver=org.h2.Driver
	quarkus.datasource.hello.url=jdbc:h2:tcp://localhost/mem:default
	quarkus.datasource.hello.username=username

	# Datasource name is "users"
	quarkus.datasource.users.driver=org.h2.Driver
	quarkus.datasource.users.url=jdbc:h2:tcp://localhost/mem:default
	quarkus.datasource.users.username=username
	```
* Specify datasource in code:
	```java
	@Inject
	@DataSource("hello")
	AgroalDataSource helloDataSource;

	@Inject
	@DataSource("users")
	AgroalDataSource usersDataSource;
	```

### Lesson 37 - Hibernate Search + Elasticsearch

* Add dependency: `io.quarkus` > `quarkus-hibernate-search-elasticsearch`
* Start Elasticsearch container:
	```shell
	docker run -it --rm=true --name elasticsearch_quarkus_test -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:7.5.0
	```
* application.properties:
	```properties
	quarkus.hibernate-search.elasticsearch.version=7
	quarkus.hibernate-search.elasticsearch.analysis.configurer=tech.donau.config.AnalysisConfigurer quarkus.hibernate-search.elasticsearch.index-defaults.lifecycle.strategy=drop-and-create
	quarkus.hibernate-search.elasticsearch.index-defaults.lifecycle.required-status=yellow
	quarkus.hibernate-search.automatic-indexing.synchronization.strategy=searchable
	```
* Configure analyzers by creating class that implements `ElasticsearchAnalysisConfigurer`
* Add `@Indexed` annotation to entities
* Spring Boot `@PostConstruct` equavalent in Quarkus is to create a method like this:
	```java
	public void onStart(@Observes StartupEvent event) {

	}
	```

### Lesson 38 - Using Infinispan Client

```
docker run -p 11222:11222 -e USER="root" -e PASS="root" infinispan/server

mvn io.quarkus:quarkus-maven-plugin:1.2.1.Final:create \
	-DprojectGroupId=tech.donau \
	-DprojectArtifactId=infinispan \
	-DclassName="tech.donau.BookResource" \
	-Dpath="/books" \
	-Dextensions="infinispan-client,resteasy-jsonb"

# properties
quarkus.infinispan-client.server-list=localhost:11222
quarkus.infinispan-client.auth-password=root
quarkus.infinispan-client.auth-username=root
quarkus.infinispan-client.auth-server-name=infinispan
quarkus.infinispan-client.auth-realm=default
quarkus.infinispan-client.client-intelligence=BASIC
```

### Lesson 39 - Using Transactions

* Transactional.TxType.REQUIRED seems to be a good default trans type
	- It is also the default and
	- normally the best option

### Lesson 40 - How to use Cache in your Application

* Dependency: `io.quarkus` > `quarkus.cache`
* Properties:
	```properties
	quarkus.cache.caffeine."weather-service".expire-after-write=20s
	```

### Lesson 43 - Reactive SQL Clients

* Add dependency: `io.quarkus` > `reactive-mysql-client`
* Properties:
	```properties
	quarkus.datasource.url=vertx-reactive:mysql://localhost:3310/hello
	quarkus.datasource.username=root
	quarkus.datasource.password=root
	```
* Sample class:
	```java
	public class BookResource {
		@Inject
		io.vertx.axle.mysqlclient.MySQLPool pool;

		public void onStart(@Observes StartupEvent event) {
			pool
				.query("drop table if exists books")
				.thenCompose(it -> pool.query("create table books (title text, pages int)"))
				.thenCompose(it -> pool.query("insert into books ('test book', 200)"))
				.toCompleatableFuture()
				.join();
		}
	}
	```
* There are `axle` and `mutiny` database pools.
	- Find out which is more official. I sustect that it might be `mutiny`.
	- The `mutiny` library is superior according to the lecturer

### Lesson 44 - MongoDB Client

* Dependencies: `mongodb-client` extension
* Run MongoDB container: `docker run -ti --rm -p 27017:27017 mongo:4.0`

### Lesson 45 - MongoDB with Panache

* Dependencies: `mongodb-panache` extension
* Sample entity:
	```java
	import io.quarkus.mongodb.panache.MongoEntity;
	import io.quarkus.mongodb.panache.PanacheMongoEntityBase;
	import org.bson.codecs.pojo.annotations.BsonId;

	@MongoEntity(collection = "books")
	public class Book extends PanacheMongoEntityBase {
	    @BsonId
    	public String title;
    	public Integer pages;
	}
	```

## Section 7 - Quarkus Messaging

### Lesson 48 - Using Apache Kafka

Create new project:

```shell
mvn io.quarkus:quarkus-maven-plugin:1.3.0.Final:create \
	-DprojectGroupId=tech.donau \
	-DprojectArtifactId=kafka \
	-Dextensions="kafka"
```

Docker compose file:

```yaml
version: '2'

services:
  zookeeper:
    image: strimzi/kafka:0.11.3-kafka-2.1.0 command: [
      "sh", "-c",
      "bin/zookeeper-server-start.sh config/zookeeper.properties" ]
    ports:
      - "2181:2181"
    environment:
	  LOG_DIR: /tmp/logs

  kafka:
    image: strimzi/kafka:0.11.3-kafka-2.1.0 command: [
      "sh", "-c",
      "bin/kafka-server-start.sh config/server.properties --override listeners=$${KAFKA_LISTENERS} -- override advertised.listeners=$${KAFKA_ADVERTISED_LISTENERS} --override zookeeper.connect=$${KAFKA_ZOOKEEPER_CONNECT}"
    ]
    depends_on:
      - zookeeper ports:
	  - "9092:9092"
	environment:
      LOG_DIR: "/tmp/logs"
	  KAFKA_ADVERTISED_LISTENERS: PLAINTEXT://localhost:9092
	  KAFKA_LISTENERS: PLAINTEXT://0.0.0.0:9092
	  KAFKA_ZOOKEEPER_CONNECT: zookeeper:2181
```

Properties:

```properties
mp.messaging.outgoing.generated-price.connector=smallrye-kafka
mp.messaging.outgoing.generated-price.topic=prices
mp.messaging.outgoing.generated-price.value.serializer=org.apache.kafka.common.serialization.LongSerializer
mp.messaging.incoming.prices.connector=smallrye-kafka
mp.messaging.incoming.prices.value.deserializer=org.apache.kafka.common.serialization.LongDeserializer
```

* Check out app called "Conduktor"

### Lesson 49 - Using AMQP with Reactive Messaging

```
mvn io.quarkus:quarkus-maven-plugin:1.2.1.Final:create \
	-DprojectGroupId=tech.donau \
	-DprojectArtifactId=amqp \
	-Dextensions="amqp"

docker run -it --rm \ -p 8161:8161 \
	-p 61616:61616 \
	-p 5672:5672 \
	-e ARTEMIS_USERNAME=root \
	-e ARTEMIS_PASSWORD=root \
	vromero/activemq-artemis
```

Properties:

```properties
amqp-username=root
amqp-password=root

mp.messaging.outgoing.price.connector=smallrye-amqp
mp.messaging.outgoing.price.address=prices
mp.messaging.outgoing.price.durable=true

mp.messaging.incoming.prices.connector=smallrye-amqp
mp.messaging.incoming.prices.durable=true
```

### Lesson 50 - Asynchronous Message Passing

* Use vert.x mutiny EventBus
* EventBus.publish
* EventBus.sendAndForget
* EventBus.request

### Lesson 51 - Using JMS with Artemis

```
mvn io.quarkus:quarkus-maven-plugin:1.3.0.Final:create \
	-DprojectGroupId=tech.donau \
	-DprojectArtifactId=jms \
	-Dextensions="qpid-jms"
```

```properties
quarkus.qpid-jms.url=amqp://localhost:5672
quarkus.qpid-jms.username=root
quarkus.qpid-jms.password=root
```

```shell
docker run -it --rm -p 8161:8161 -p 61616:61616 -p 5672:5672 -e ARTEMIS_USERNAME=root -e ARTEMIS_PASSWORD=root vromero/activemq-artemis:2.11.0-alpine
```

## Section 8 - Security

### Lesson 52 - Using OpenID Connect to Protect JAX-RS Applications

```shell
mvn io.quarkus:quarkus-maven-plugin:1.3.0.Final:create \
	-DprojectGroupId=tech.donau \
	-DprojectArtifactId=openid-connect \
	-Dextensions="oidc, resteasy-jsonb"

docker run \
	--name keycloak \
	-e KEYCLOAK_USER=admin \
	-e KEYCLOAK_PASSWORD=admin \
	-p 8180:8080 jboss/keycloak
```

```properties
quarkus.oidc.auth-server-url=http://localhost:8180/auth/realms/quarkus
quarkus.oidc.client-id=backend-service

# If webapp
quarkus.oidc.application-type=web-app
quarkus.http.auth.permission.authenticated.paths=/*
quarkus.http.auth.permission.authenticated.policy=authenticated
```

### Lesson 53 - Using Keycloak to centralize your authorization

```
mvn io.quarkus:quarkus-maven-plugin:1.3.0.Final:create \
	-DprojectGroupId=tech.donau \
	-DprojectArtifactId=openid-connect \
	-Dextensions="oidc, resteasy-jsonb"

docker run \
	--name keycloak \
	-e KEYCLOAK_USER=admin \
	-e KEYCLOAK_PASSWORD=admin \
	-p 8180:8080 jboss/keycloak
```

```properties
quarkus.oidc.auth-server-url=http://localhost:8180/auth/realms/quarkus
quarkus.oidc.client-id=backend-service

# If webapp
quarkus.oidc.application-type=web-app
quarkus.http.auth.permission.authenticated.paths=/*
quarkus.http.auth.permission.authenticated.policy=authenticated
```

```
./mvnw quarkus:add-extension -Dextension="keycloak-authorization" quarkus.oidc.credentials.secret=secret

quarkus.keycloak.policy-enforcer.enable=true
```

### Lesson 55 - Using JWT RBAC

```shell
openssl genrsa -out publicKey.pem
openssl pkcs8 -topk8 -inform PEM -in publicKey.pem -out privateKey.pem -nocrypt
openssl rsa -in publicKey.pem -pubout -outform PEM -out publicKey.pem 
```

```properties
mp.jwt.verify.publickey.location=META-INF/resources/publicKey.pem
mp.jwt.verify.issuer=DonauTech
quarkus.smallrye-jwt.enabled=true
```

```java
# generate token
JwtClaims jwtClaims = new JwtClaims();
jwtClaims.setIssuer("DonauTech");
jwtClaims.setJwtId("a-123");
jwtClaims.setSubject(email);
jwtClaims.setClaim(Claims.upn.name(), email);
jwtClaims.setClaim(Claims.preferred_username.name(), username);
jwtClaims.setClaim(Claims.birthdate.name(), birthdate);
jwtClaims.setClaim(Claims.groups.name(), Arrays.asList(TokenUtils.ROLE_USER));
jwtClaims.setAudience("using-jwt");
jwtClaims.setExpirationTimeMinutesInTheFuture(1);

String token = TokenUtils.generateTokenString(jwtClaims);
```

```shell
# Project
mvn io.quarkus:quarkus-maven-plugin:1.3.0.Final:create \
	-DprojectGroupId=tech.donau \
	-DprojectArtifactId=security-jwt \
	-DclassName="tech.donau.ProfileResource" \
	-Dpath="/profile" \
	-Dextensions="resteasy-jsonb, jwt"
```

### Lesson 56 - Using Security with JDBC Realm

Lesson notes:

```
Use security-jpa as initial setup

docker run --name quarkus-postgres -e POSTGRES_PASSWORD=root -e POSTGRES_USER=root -p 5432:5432 -d postgres

#posgresql
quarkus.datasource.db-kind=postgresql
quarkus.datasource.username=root
quarkus.datasource.password=root
quarkus.datasource.jdbc.url=jdbc:postgresql://localhost:5432/quarkus

quarkus.security.jdbc.enabled=true
quarkus.security.jdbc.principal-query.sql=SELECT u.password, u.role FROM clients u WHERE u.username=?
quarkus.security.jdbc.principal-query.enabled=true
quarkus.security.jdbc.principal-query.clear-password-mapper.password-index=1
quarkus.security.jdbc.principal-query.attribute-mappings.0.index=2
quarkus.security.jdbc.principal-query.attribute-mappings.0.to=groups
```

### Lesson 57 - Using Security with JPA

Lesson notes:

```
mvn io.quarkus:quarkus-maven-plugin:1.3.0.Final:create \
	-DprojectGroupId=tech.donau \
	-DprojectArtifactId=security-jpa \
	-Dextensions="security-jpa, jdbc-postgresql, resteasy, hibernate-orm-panache"

docker run --name quarkus-postgres -e POSTGRES_PASSWORD=root -e POSTGRES_USER=root -p 5432:5432 -d postgres

#mysql
quarkus.datasource.db-kind=mysql
quarkus.datasource.username=root
quarkus.datasource.password=root
quarkus.datasource.jdbc.url=jdbc:mysql://localhost:3310/hello
quarkus.hibernate-orm.database.generation=drop-and-create

#posgresql
quarkus.datasource.db-kind=postgresql
quarkus.datasource.username=quarkus
quarkus.datasource.password=quarkus
quarkus.datasource.jdbc.url=jdbc:postgresql:security_jpa
quarkus.hibernate-orm.database.generation=drop-and-create
```

### Lesson 58 - Security with Properties

Lesson notes

```
mvn io.quarkus:quarkus-maven-plugin:1.2.1.Final:create \
	-DprojectGroupId=tech.donau \
	-DprojectArtifactId=security-properties \
	-DclassName="tech.donau.UserResource" \
	-Dpath="/user" \
	-Dextensions="elytron-security-properties-file"

quarkus.security.users.file.enabled=true
quarkus.security.users.file.users=users.properties
quarkus.security.users.file.roles=roles.properties
quarkus.security.users.file.realm-name=QuarkusRealm
quarkus.security.users.file.plain-text=true
```

## Section 9 - Quarkus Cloud

### Lesson 59 - Generating Kubernetes Resources

Lesson notes:

```
mvn io.quarkus:quarkus-maven-plugin:1.2.1.Final:create \
	-DprojectGroupId=tech.donau \
	-DprojectArtifactId=containers \
	-DclassName="tech.donau.WeatherResource" \
	-Dpath="/weather" \
	-Dextensions="container-image-jib"

docker run -d -p 5000:5000 --restart always --name registry registry:2

# Configs
quarkus.container-image.registry=http://localhost:5000
```

* To build container package:
	```
	./mvnw package -Dquarkus-container-image.build=true
	```
* More properties:
	```properties
	quarkus.container-image.registry=localhost:5000
	quarkus.container-image.insecure=true
	```
* Push the image to your registry:
	```shell
	./mvnw package -Dquarkus.container-image.push=true
	```
* View local registry:
	```
	http://localhost:5000/v2/_catalog
	```

### Lesson 60 - Deploying Native Applications on Kubernetes

Lesson notes:

```
mvn io.quarkus:quarkus-maven-plugin:1.3.1.Final:create \
	-DprojectGroupId=tech.donau \
	-DprojectArtifactId=quarkus-kubernetes \
	-DclassName="tech.donau.WeatherResource" \
	-Dpath="/weather" \
	-Dextensions="kubernetes, docker"

# To use with minikube:
eval $(minikube docker-env)

deploy with ./mvnw

kubectl expose deployment kubernetes --type=LoadBalancer --name=kubernetes-service minikube service kubernetes-service

# config
quarkus.kubernetes.image-pull-policy=ifNotPresent
quarkus.kubernetes.service-type=LoadBalancer
```

* Start minikube
	```
	minikube start
	```
* Deploy to Kubernetes:
	```
	./mvnw clean package -Dquarkus.kubernetes.deploy=true
	```

### Lesson 61 - Deploying Quarkus on Google Cloud Platform GKE

* Some properties:
	```properties
	quarkus.kubernetes.image-pull-policy=ifNotPresent
	quarkus.kubernetes.replicas=3
	quarkus.kubernetes.env-vars.test.value=test
	quarkus.kubernetes.env-vars.key.value=value
	quarkus.container-image.registry=localhost:5000
	quarkus.container-image.group=yourGroup
	```

### Lesson 62 - Deploying Native Applications on Knative Kubernetes

Lesson notes:

```
mvn io.quarkus:quarkus-maven-plugin:1.3.1.Final:create \
	-DprojectGroupId=tech.donau \
	-DprojectArtifactId=quarkus-knative \
	-DclassName="tech.donau.WeatherResource" \
	-Dpath="/weather" \
	-Dextensions="kubernetes, docker"

# To use with minikube:
eval $(minikube docker-env)

# Configs
quarkus.kubernetes.deployment-target=knative

cp src/main/docker/Dockerfile.jvm Dockerfile deploy with ./mvnw

kubectl expose deployment kubernetes --type=LoadBalancer --name=kubernetes-service

minikube service kubernetes-service

quarkus.kubernetes.deployment-target=knative
quarkus.knative.image-pull-policy=ifNotPresent
quarkus.container-image.registry=eu.gcr.io
quarkus.container-image.group=driven-atrium-259009
```

### Lesson 63 - Using the Kubernetes Client to Interact with a Kubernetes Cluster

* Add dependency: `io.quarkus` > `quarkus-kubernetes-client`

### Lesson 64 - Azure Functions (Serverless) with Vert.x Web, Servlet, or Resteasy

Lesson notes:

```
mvn archetype:generate \
	-DarchetypeGroupId=io.quarkus \
	-DarchetypeArtifactId=quarkus-azure-functions-http-archetype \
	-DarchetypeVersion=1.3.1.Final
	
./mvnw clean install azure-functions:deploy
```

### Lesson 65 - AWS Lambda

Lesson notes:

```
mvn archetype:generate \
	-DarchetypeGroupId=io.quarkus \
	-DarchetypeArtifactId=quarkus-amazon-lambda-http-archetype \
	-DarchetypeVersion=1.3.1.Final

# Local
sam local start-api --template sam.jvm.yaml

# AWS
sam package --template-file sam.jvm.yaml --output-template-file packaged.yaml --s3-bucket Ð–
sam deploy --template-file packaged.yaml --capabilities CAPABILITY_IAM --stack-name
```

* Add dependency: `io.quarkus` > `quarkus-amazon-lambda-http`
* `sam` is amazon tool

## Section 10 - Quarkus Observability

### Lesson 66 - Using Health Check

Lesson notes

```
mvn io.quarkus:quarkus-maven-plugin:1.3.0.Final:create \
	-DprojectGroupId=tech.donau \
	-DprojectArtifactId=microprofile-health \
	-Dextensions="health"
```

Basic readiness health check implementation:

```java
@Readiness
@ApplicationScoped
public class ReadinessCheck implements HealthCheck {
	@Override
	public HealthCheckResponse call() {
		return null;
	}
}
```

Basic liveness health check implementation:

```java
@Liveness
@ApplicationScoped
public class ReadinessCheck implements HealthCheck {
	@Override
	public HealthCheckResponse call() {
		return HealthCheckResponse
			.builder()
			.name("My name")
			.up()
			.withData("someKey", "someValue")
			.build();
	}
}
```

* `@Readiness` - to indicate that the app is ready
* `@Liveleness` - to indicate stack liveness
* Readiness endpoint: `/health/ready`
* Liveness endpoint: `/health/live`

### Lesson 67 - Using OpenTracing

Lesson notes

```
docker run -p 5775:5775/udp -p 6831:6831/udp -p 6832:6832/udp -p 5778:5778 -p 16686:16686 -p 14268:14268jaegertracing/all-in-one:latest

mvn io.quarkus:quarkus-maven-plugin:1.3.0.Final:create \
	-DprojectGroupId=tech.donau \
	-DprojectArtifactId=opentracing \
	-DclassName="tech.donau.opentracing.TracedResource" \
	-Dpath="/hello" \
	-Dextensions="quarkus-smallrye-opentracing"

quarkus.jaeger.service-name=myservice
quarkus.jaeger.sampler-type=const
quarkus.jaeger.sampler-param=1
quarkus.log.console.format=%d{HH:mm:ss} %-5p traceId=%X{traceId}, spanId=%X{spanId}, sampled=%X{sampled}[%c{2.}] (%t) %s%e%n

io.opentracing.contrib
opentracing-jdbc
```

* Note: OpenTracking looks cool. Check it out some more.

### Lesson 68 - Sentry

Sentry reminds me of DataDog. Maybe check it out.

### Lesson 69 - Collecting Metrics

* Use `@Timed`, `@Counted`, and `@Gauge` annotations to collect metrics
* Endpoint for raw metrics: `/metrics/application`

### Lesson 70 - GELF

* Send logs to greylog, elasticsearch, and others

Lesson notes:

```
mvn io.quarkus:quarkus-maven-plugin:1.3.0.Final:create \
	-DprojectGroupId=tech.donau \
	-DprojectArtifactId=gelf \
	-DclassName="tech.donau.HelloResource" \
	-Dpath="/hello" \
	-Dextensions="logging-gelf"

quarkus.log.handler.gelf.enabled=true
quarkus.log.handler.gelf.host=localhost
quarkus.log.handler.gelf.port=12201

# Graylog
version: '3.2'
services:
  elasticsearch:
    image: docker.elastic.co/elasticsearch/elasticsearch-oss:6.8.2
	ports:
	  - "9200:9200"
	environment:
	  ES_JAVA_OPTS: "-Xms512m -Xmx512m"
	networks:
	  - graylog
	  
  mongo:
    image: mongo:4.0
	networks:
	  - graylog
	  
  graylog:
    image: graylog/graylog:3.1
	ports:      
	  - "9000:9000"
	  - "12201:12201/udp"
	  - "1514:1514"
	environment:
	  GRAYLOG_HTTP_EXTERNAL_URI: "http://127.0.0.1:9000/"    
	networks:
	  - graylog
	depends_on:
	  - elasticsearch
	  - mongo

networks:
  graylog:
    driver: bridge

---

curl -H "Content-Type: application/json" -H "Authorization: Basic YWRtaW46YWRtaW4=" -H "X-Requested-By: curl" -X POST -v -d \'{"title":"udp input","configuration":{"recv_buffer_size":262144,"bind_address":"0.0.0.0","port":12201,"decompress_size_limit":8388608},"type":"org.graylog2.inputs.gelf.udp.GELFUDPInput","global":true}' \http://localhost:9000/api/system/inputs

# logstash
Create file ./pipelines/gelf.conf
```
