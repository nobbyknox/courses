# Quarkus

https://www.udemy.com/share/1032zCBUEfeVtaQH4=/

Access to `coursebox`:

```shell
$ ssh -i C:\Users\QXZ198Y\data\keys\course_key nobby@64.225.21.128
```

Better yet, add the following to your `~/.ssh/config` file:

```
Host coursebox
    HostName 64.225.21.128
    User nobby
    IdentityFile "/home/nobby/dev/courses/keys/course_key"
```

and then:

```shell
$ ssh coursebox
```
